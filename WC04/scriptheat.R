#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

d <- as.matrix(read.csv(args[1], header = FALSE, sep = ",")[-1,-1])
rownames(d) <- read.csv(args[1], header=FALSE, sep=",")[-1,1]
colnames(d) <- read.csv(args[1], header=FALSE, sep=",")[1,-1]

jpeg(file="/home/hans/Desktop/Repos/snakemake/WC04/heatmap.jpg")
heatmap(as.matrix(d))
dev.off()