# -*- python -*-
from os.path import join
# SAMPLES = ["A", "B", "C"]

configfile: "config.yaml"
workdir: "/home/hans/Desktop/Repos/snakemake/WC02"
DATA_DIR = "data/"
REF_INDEX = "data/reference_index"



rule all:
    input:
        "out.html"

rule bowtie2Build:
    input:
         join(DATA_DIR, "genome.fa")
    params:
        basename = join(REF_INDEX, "genome")
    output:
          output1 = join(REF_INDEX, "genome.1.bt2")
    message:
          "Building bowtie2 index with genome input: {input}"

    shell:
        "bowtie2-build {input} {params.basename}"



rule bowtie2:
    input:
         smp = "data/samples/{sample}.fastq",
         index = join(REF_INDEX,"genome.1.bt2")
    params:
        idx=join(REF_INDEX,"genome")

    output:
          "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bowtie.benchmark.txt"

    message:
        "Executing Bowtie2 with this out: {output}"
    threads: 8
    shell:
        "bowtie2 -x {params.idx} -U  {input.smp} -S {output}"




rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    message:
        "Sorting reads using samtools"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"




rule samtools_index:
    input:
         "sorted_reads/{sample}.bam"
    output:
          "sorted_reads/{sample}.bam.bai"
    message:
          "Indexing read alignments for random access"
    shell:
         "samtools index {input}"


rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("sorted_reads/{sample}.bam", sample=config["samples"]),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "calls/all.vcf"
    message:
        "Aggregating mappeed reads from all samples and calling genomic variants"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"



rule report:
    input:
        T1 ="calls/all.vcf",
        T2 = expand("benchmarks/{sample}.bowtie.benchmark.txt", sample=config["samples"])
    output:
        "out.html"
    message:
        "Analyzing and reporting data, generating html file"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast
        reference genome and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        Benchmark results for BWA can be found in the tables T2_.
        """, output[0], **input)





