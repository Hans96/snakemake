# Snakemake metagenomics pipeline #

This pipeline filters metagenomic reads by mapping the reads against a reference genome. The remaining reads are assembled and mapped by using blast to a bacterial database
With the remaining reads a barplot is produced that shows the top twenty organisms with the most blast hits. besides the amount of hits the organisms are filtered based on evalue of 0, 100% identical match and no gaps. 
This pipeline is written with Snakemake and python3. Snakemake is a workflow management system, designed to streamline the execution of software pipelines. 

### Requirements ###

Snakemake requires python3. It is recommended that you create a python3 virtual environment where the required packages are installed. 
The required packages for this pipeline are snakemake, matplotlib, numpy and Biopython. The setup for the virtual environment and the installation of snakemake and packages can be done by executing
the following commands on the command line:




1.virtualenv -p /usr/bin/python3 venv

2.source venv/bin/activate

3.pip3 install snakemake matplotlib numpy Biopython



Furthermore, this pipeline uses Trimmomatic. Trimmomatic can be downloaded [here](http://www.usadellab.org/cms/?page=trimmomatic). For the assembly of left over reads (after mapping to the reference genome) is done by [Spades](http://cab.spbu.ru/software/spades/)
after extraction and installation it is important to tell snakemake where trimmomatic and spades are installed. This will be explained next.

### Configuring config.yaml ###

The rules for the Snakemake tasks are defined in the [Snakefile](eindopdracht/Snakefile).

Configuration parameters for this Snakefile are read from the YAML file [config.yaml](eindopdracht/config.yaml)

Before running Snakemake, edit this file to specify the location of all of the input directories and files that will be used by the pipeline. This includes locations of the trimmomatic or spades, fastqc files, input BAM files etc.

### Running snakemake ###
if called without parameters:

$ snakemake

Snakemake tries to execute the workflow specified in a file called Snakefile in the same directory

Snakemake can be run on multiple threads in order to use multithreading the config yaml needs to be adjusted to tell snakemake how many cores should be used for every rule.
after adjusting the config.yaml enter the followiung command in the terminal.

$ snakemake --cores [amountofcores] --snakefile Snakefile

### Debugging snakemake jobs ###

For every job snakemake will write an output file for each job to a directory called logs (directory location adjustable in config.yaml)
These files will be named to the rule that executed them <snakerulename>.log. If a rule fails, you should check the appropriate output file to see what error occurred. When a job fails and a re-run is done snakemake will pickup where it left off.


