#!/usr/bin/env python3


'''
Creates a barplot of the species with the most blast hits
'''

import sys
import numpy as np
import matplotlib.pyplot as plt


def get_data(data):
	'''
	parses the Genbank id and scientific name of bacterial species
	'''
	species = list()
	for line in data:
		species.append(line.split(";")[0] + " " + " ".join(line.split(";")[1].split(" ")[0:2]))
	return species
		
# def map_id_to_specie(names):
# 	'''Maps Genbank Id to the specie name'''
# 	species = dict()
# 	for name in names:
# 		gb_id = name.split(" ")[0]
# 		specie = name.split(" ")[1] + " " + name.split(" ")[2]
# 		if gb_id not in species:
# 			species[gb_id] = specie
# 	return species
	
def count_unique_species(names):
	'''Counts the amount of blast hits for every specie'''
	species = dict()
	for name in names:
		gb_id = name.split(" ")[0]
		specie = name.split(" ")[1] + " " + name.split(" ")[2]
		if specie in species:
			species[specie] += 1
		else:
			species[specie] = 1
	return species
	
def plot_unique_species_hits(species):
	'''creates a barplot of the top twenty species with the most blast hits'''
	species_sorted = sorted(species.items(), key=lambda kv: kv[1], reverse=True)[0:19]
	x, y = zip(*species_sorted)
	plt.bar(x,y)
	plt.title("Blast hits for twenty (oral) bacterial species with 100% identity and no gaps")
	plt.ylabel("Amount of blast hits")
	plt.xlabel("Bacterial species")
	plt.xticks(rotation=90)
	plt.tight_layout()
	plt.savefig("barplot.png")

	
		
def main(args):
	'''main method executes script'''
	# print(args[0])
	fileopen = open(args[1])
	get_species = get_data(fileopen)
	count_species = count_unique_species(get_species)
	plot_species = plot_unique_species_hits(count_species)
	

if __name__ == "__main__":
	exitcode = main(sys.argv)
	sys.exit(exitcode)
