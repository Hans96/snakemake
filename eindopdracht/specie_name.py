#!/usr/bin/env python3

'''
Fetches scientific name of bacteria based on Genbank id
from NCBI database, bacteria with no gaps, mismatches and an evalue of 0 are selected
'''

from Bio import Entrez, SeqIO
import sys


def read_file(openfile):
    '''Reads the blast output file and takes the best matching blast hits, based on gaps,mismatch evalue and percent match'''
    ids = list()
    for line in openfile:
        percent_match = float(line.split("\t")[2])
        gaps = float(line.split("\t")[4])
        mismatch = float(line.split("\t")[5])
        evalue = float(line.split("\t")[10])
        if percent_match == 100 and gaps == 0 and mismatch == 0 and evalue == 0:
            ids.append(line.split("\t")[1][9:])
    return ids


def get_specie(ids, openfile):
    '''fetches organism name from NCBI database and writes name with genbank id to output file'''''
    for gb_id in ids:
        print(gb_id)
        try:
            fa = Entrez.esummary(db="nucleotide", id=gb_id, retmode="xml")
            records = Entrez.parse(fa)
            for record in records:
                openfile.write(gb_id + ";" + record["Title"] + "\n")
        except:
            pass



def main(args):
    '''main function that executes script'''
    Entrez.email = 'hanszijlstra@msn.com'
    blastfile = open(args[1])
    a = read_file(blastfile)
    b = get_specie(a, open(args[2], "w"))
    

if __name__ == "__main__":
    exitcode = main(sys.argv)
    sys.exit(exitcode)
